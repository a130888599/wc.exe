#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int CCount(char file[])
{
    FILE *pf = NULL;
    int ccount = 0;
    pf = fopen(file, "r");
    if (pf == NULL)
    {
        printf("未寻找到目标文件\n");
        exit(-1);
    }
    char mychar;
        mychar = fgetc(pf);
    while (mychar != EOF)
    {
        if(mychar != '\n' && mychar != '\r' && mychar != ' ') {
            ccount++;
        }
        mychar = fgetc(pf);
    }
    fclose(pf);
    return ccount;
}
int WCount(char file[])
{
    FILE *pf = NULL;
    int wcount = 0;
    pf = fopen(file, "r");
    if (pf == NULL)
    {
        printf("未寻找到目标文件\n");
        exit(-1);
    }
    char mychar;
    mychar = fgetc(pf);
    int isSpace = 1;
    while (mychar != EOF)
    {
        if(mychar == ' ' || mychar == '\n')
            isSpace = 1;
        else {
            if(isSpace)
                wcount ++;
            isSpace = 0;
        }
        mychar = fgetc(pf);
    }
    fclose(pf);
    return wcount;
}
int LCount(char file[])
{
    FILE *pf = NULL;
    int lcount = 0;
    pf = fopen(file, "r");
    if (pf == NULL)
    {
        printf("未寻找到目标文件\n");
        exit(-1);
    }
    char mychar;
    mychar = fgetc(pf);
    while (mychar != EOF)
    {
        if (mychar == '\n')
        {
            lcount++;
            mychar = fgetc(pf);
        }
        else
        {
            mychar = fgetc(pf);
        }
    }
    fclose(pf);
    return lcount + 1;
}
int main()
{ //
    char input[20], File[200];
    while (1)
    {
        printf("请输入用户命令：wc.exe-");
        scanf("%s", &input);
        if (input[0] == 'c')
        {
            printf("请输入文字名：");
                scanf("%s", &File);
            int charcount = 0;
            charcount = CCount(File);
            printf("文件的字符数为：%d\n", charcount);
            continue;
        }
        if (input[0] == 'w')
        {
            printf("请输入文件名：");
                scanf("%s", &File);
            int wordcount = 0;
            wordcount = WCount(File);
            printf("文件的词数为：%d\n", wordcount);
            continue;
        }
        if (input[0] == 'l')
        {
            printf("请输入文件名：");
                scanf("%s", &File);
            int lcount = 0;
            lcount = LCount(File);
            printf("文件的行数为：%d\n", lcount);
            continue;
        }
    }
    system("pause");
    return 0;
}
